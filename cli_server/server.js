var http = require('http');
var url = require('url');
var items = [];

var server = http.createServer(function(req, res){
	switch (req.method){
		case 'POST':
			var item = '';
			req.setEncoding('utf8');
			req.on('data', function(chunk){
				item += chunk;
				console.log('botman1 is receiving data', chunk);
			});
			req.on('end', function(){
				items.push(item);
				console.log('botman1 is done receiving data');
				res.end('Botman1 was successfully assigned a task\n');
			});
			break;
		case 'GET':
			var body = items.map(function(item, i){
					return i + ') ' + item; 
			}).join('\n');
			body += '\nBotman1 has the above tasks\n';
			res.setHeader('Content-Length', Buffer.byteLength(body));
			res.setHeader('Content-Type', 'text/plain; charset="utf-8"');
			res.end(body);
			break;
		case 'DELETE': //look for a DELETE http request
			//curl -X DELETE http://localhost:3000/0
				var path = url.parse(req.url).pathname;
				var i = parseInt(path.slice(1), 10);

				if(isNaN(i)){
					res.statusCode = 400;
					res.end('\nBotman1 believes this is an Invalid item ID because the parsed item ID is not a number');
				} else if (!items[i]){
					res.statusCode = 404;
					res.end('\nBotman1 believes this item ID does not point to an item in the items array');
				} else {
					items.splice(i, 1);
					res.end('\nBotman1 has successfully deleted the item specified.\n');
				}
				break;
			case 'PUT': //look for a PUT http request
			//curl -X PUT http://localhost:3000/0?"do dishes"
				var path = url.parse(req.url).pathname;
				var i = parseInt(path.slice(1), 10);

				var item = url.parse(req.url).query;

				if(isNaN(i)){
					res.statusCode = 400;
					res.end('Invalid item id');
				} else if (!items[i]){
					res.statusCode = 404;
					res.end('Item not found');
				} else {
					items[i] = item;
					res.end('OK\n');
				}
				break;
	}
}).listen(3000);